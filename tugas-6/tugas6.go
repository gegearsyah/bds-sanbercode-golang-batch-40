package main

import "fmt"

var luasLingkaran float64
var kelilingLingkaran float64

// Soal 1
func pointerLingkaran(jari_jari float64) {
	*&luasLingkaran = 22 / 7 * jari_jari * jari_jari
	*&kelilingLingkaran = 22 / 7 * jari_jari * 2
}

// Soal 2
func introduce(sentence *string, nama string, jk string, pekerjaan string, usia string) {
	if jk == "laki-laki" {
		*sentence = "Pak " + nama + " adalah " + "seorang " + pekerjaan + " yang berusia " + usia + " tahun"
	} else if jk == "perempuan" {
		*sentence = "Bu " + nama + " adalah " + "seorang " + pekerjaan + " yang berusia " + usia + " tahun"
	}

	return
}

// Soal 3
func tambahBuah(buah *[]string) {
	*buah = append(*buah, "Jeruk")
	*buah = append(*buah, "Mangga")
	*buah = append(*buah, "Semangka")
	*buah = append(*buah, "Strawberry")
	*buah = append(*buah, "Durian")
	*buah = append(*buah, "Manggis")
	*buah = append(*buah, "Alpukat")
}

func tambahDataFilm(film string, waktu string, genre string, tahun string, dataFilm *[]map[string]string) {
	data := map[string]string{}

	data["genre"] = genre
	data["jam"] = waktu
	data["tahun"] = tahun
	data["title"] = film

	*dataFilm = append(*dataFilm, data)

}

func main() {
	// Soal 1
	var jari_jari float64 = 8
	pointerLingkaran(jari_jari)
	fmt.Println(luasLingkaran)
	fmt.Println(kelilingLingkaran)

	// Soal 2
	var sentence string
	introduce(&sentence, "John", "laki-laki", "penulis", "30")

	fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
	introduce(&sentence, "Sarah", "perempuan", "model", "28")

	fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	// Soal 3

	var buah = []string{}
	tambahBuah(&buah)
	for index, value := range buah {
		fmt.Printf("%d. %s \n", index+1, value)
	}

	// Soal 4

	var dataFilm = []map[string]string{}

	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

	for index, item := range dataFilm {
		lengthofIndex := 0
		for indexItem, value := range item {
			if lengthofIndex == 0 {
				fmt.Printf("%d. %s: %s\n", index+1, indexItem, value)
			} else {
				fmt.Printf("   %s: %s\n", indexItem, value)
			}
			lengthofIndex++
		}

	}
}
