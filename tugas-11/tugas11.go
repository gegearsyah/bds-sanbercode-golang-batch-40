package main

import (
	"fmt"
	"sort"
	"sync"
	"time"
)

// Soal 1
func cetakPhones(number string, phone string, wg *sync.WaitGroup) {
	fmt.Println(number, phone)
	time.Sleep(time.Second * 1)
	wg.Done()
}

// Tugas 2
func getMovies(channel chan string, movies ...string) {
	for _, movie := range movies {
		channel <- movie
	}
	close(channel)
}

func main() {
	// Tugas 1
	var wg sync.WaitGroup
	var phones = []string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}
	sort.Strings(phones)

	for index, value := range phones {
		number := index + 1
		increment := fmt.Sprintf("%d.", number)
		wg.Add(1)
		cetakPhones(increment, value, &wg)
	}
	wg.Wait()

	// Tugas 2
	var movies = []string{"Harry Potter", "LOTR", "SpiderMan", "Logan", "Avengers", "Insidious", "Toy Story"}

	moviesChannel := make(chan string)

	go getMovies(moviesChannel, movies...)

	for value := range moviesChannel {
		fmt.Println(value)
	}

}
