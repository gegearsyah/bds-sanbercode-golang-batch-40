package main

import (
	"fmt"
	"tugas9/module"
)

func main() {

	// Soal 1
	var dataSegitigaSamaSisi module.HitungBangunDatar = &module.SegitigaSamaSisi{
		Alas:   12,
		Tinggi: 13,
	}
	dataSegitigaSamaSisi.Luas()
	dataSegitigaSamaSisi.Keliling()

	var dataPersegiPanjang module.HitungBangunDatar = &module.PersegiPanjang{
		Panjang: 12,
		Lebar:   13,
	}
	dataPersegiPanjang.Luas()
	dataPersegiPanjang.Keliling()

	var dataTabung module.HitungBangunRuang = &module.Tabung{
		JariJari: 12,
		Tinggi:   13,
	}
	dataTabung.Volume()
	dataTabung.LuasPermukaan()

	var dataBalok module.HitungBangunRuang = &module.Balok{
		Panjang: 12,
		Lebar:   13,
		Tinggi:  3,
	}
	dataBalok.Volume()
	dataBalok.LuasPermukaan()

	// Soal 2
	var dataPhone module.BrandPrint = &module.Phone{
		Name:   "Samsung Galaxy Note 20",
		Brand:  "Samsung Galaxy Note 20",
		Year:   200,
		Colors: []string{"Mystic Bronze", "Mystic White", "Mystic Black"},
	}
	hasilCetak := dataPhone.CetakHandphone()
	fmt.Println(hasilCetak)

	// Soal 3
	fmt.Println(module.LuasPersegi(4, true))
	fmt.Println(module.LuasPersegi(8, false))
	fmt.Println(module.LuasPersegi(0, true))
	fmt.Println(module.LuasPersegi(0, false))

	// Soal 4
	module.PrefixOfNumber()
}
