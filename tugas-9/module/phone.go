package module

import (
	"fmt"
	"strconv"
)

type Phone struct {
	Name, Brand string
	Year        int
	Colors      []string
}

type BrandPrint interface {
	CetakHandphone() string
}

func (p Phone) CetakHandphone() string {
	name := "name : " + p.Name
	brand := "brand : " + p.Brand
	year := "year : " + strconv.Itoa(p.Year)
	colors := "colors : "
	for index, value := range p.Colors {
		if index == len(p.Colors)-1 {
			colors = colors + value
		} else {
			colors = colors + value + ","
		}

	}

	return fmt.Sprintf("%s\n%s\n%s\n%s\n", name, brand, year, colors)

}
