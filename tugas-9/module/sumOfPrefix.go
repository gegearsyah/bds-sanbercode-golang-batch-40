package module

import (
	"fmt"
	"strings"
)

func PrefixOfNumber() {
	var prefix interface{} = "hasil penjumlahan dari "

	var kumpulanAngkaPertama interface{} = []int{6, 8}

	var kumpulanAngkaKedua interface{} = []int{12, 14}

	// tulis jawaban anda disini
	sumAngkaPertama := 0
	sumAngkaKedua := 0

	for _, value := range kumpulanAngkaPertama.([]int) {
		sumAngkaPertama = sumAngkaPertama + value
	}

	for _, value := range kumpulanAngkaKedua.([]int) {
		sumAngkaKedua = sumAngkaKedua + value
	}

	hitungAngkaPertama := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(kumpulanAngkaPertama)), " + "), "[]")
	hitungAngkaKedua := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(kumpulanAngkaKedua)), " + "), "[]")

	fmt.Println(prefix, hitungAngkaPertama, "+", hitungAngkaKedua, "=", sumAngkaPertama+sumAngkaKedua)
}
