package module

import "strconv"

func LuasPersegi(angka int, boolean bool) interface{} {

	var data interface{}
	if boolean {
		if angka == 0 {
			data = "Maaf anda belum menginput sisi dari persegi"
		} else {
			data = "Luas persegi dengan sisi " + strconv.Itoa(angka) + " cm adalah " + strconv.Itoa(angka*angka) + " cm"
		}
	} else {
		if angka == 0 {
			data = nil
		} else {
			data = angka
		}
	}
	return data
}
