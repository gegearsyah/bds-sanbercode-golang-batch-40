package module

import "fmt"

// Soal 1
type SegitigaSamaSisi struct {
	Alas, Tinggi int
}

func (s SegitigaSamaSisi) Luas() int {
	Luas := (s.Alas * s.Tinggi)

	fmt.Println("Luas Segitiga Sama Sisi :", Luas)

	return Luas
}

func (s SegitigaSamaSisi) Keliling() int {
	Keliling := 3 * s.Alas

	fmt.Println("Keliling Segitiga Sama Sisi :", Keliling)
	return Keliling
}

type PersegiPanjang struct {
	Panjang, Lebar int
}

func (p PersegiPanjang) Luas() int {
	Luas := p.Panjang * p.Lebar
	fmt.Println("Luas Persegi Panjang :", Luas)
	return Luas

}

func (p PersegiPanjang) Keliling() int {
	Keliling := (2 * p.Panjang) + (2 * p.Lebar)
	fmt.Println("Keliling Persegi Panjang :", Keliling)
	return Keliling
}

type HitungBangunDatar interface {
	Luas() int
	Keliling() int
}
