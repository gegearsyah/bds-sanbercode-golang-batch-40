package module

import (
	"fmt"
)

type Tabung struct {
	JariJari, Tinggi float64
}

func (t Tabung) Volume() float64 {
	Volume := (22 * t.JariJari * t.JariJari * t.Tinggi) / 7
	fmt.Println("Volume Tabung :", Volume)
	return Volume
}

func (t Tabung) LuasPermukaan() float64 {
	LuasPermukaan := (2 * 22 * t.JariJari * (t.JariJari + t.Tinggi)) / 7
	fmt.Println("Luas Permukaan Tabung :", LuasPermukaan)
	return LuasPermukaan
}

type Balok struct {
	Panjang, Lebar, Tinggi int
}

func (b Balok) Volume() float64 {
	Volume := b.Lebar * b.Panjang * b.Tinggi
	fmt.Println("Volume Balok :", Volume)
	return float64(Volume)
}

func (b Balok) LuasPermukaan() float64 {
	LuasPermukaan := 2 * ((b.Panjang * b.Lebar) + (b.Lebar * b.Tinggi) + (b.Panjang * b.Tinggi))
	fmt.Println("Luas Permukaan Balok :", LuasPermukaan)
	return float64(LuasPermukaan)
}

type HitungBangunRuang interface {
	Volume() float64
	LuasPermukaan() float64
}
