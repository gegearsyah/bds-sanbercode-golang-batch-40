package main

import "fmt"

// Soal 1
type buah struct {
	Nama       []string
	Warna      []string
	AdaBijinya []bool
	Harga      []int64
}

// Soal 2

type segitiga struct {
	alas, tinggi int
}

type persegi struct {
	sisi int
}

type persegiPanjang struct {
	panjang, lebar int
}

func (s segitiga) luasSegitiga() {
	luasSegita := (s.alas * s.tinggi) / 2
	fmt.Println(luasSegita)
}

func (s persegi) luasPersegi() {
	luasPersegi := s.sisi * s.sisi
	fmt.Println(luasPersegi)
}

func (s persegiPanjang) luasPersegiPanjang() {
	luasPersegiPanjang := s.panjang * s.lebar
	fmt.Println(luasPersegiPanjang)
}

// Soal 3
type phone struct {
	name, brand string
	year        int
	colors      []string
}

func (s *phone) colorsProperty(color []string) {
	s.colors = color
}

// Soal 4
type movie struct {
	name, genre string
	jam, tahun  int64
}

func tambahDataFilm(name string, waktu int64, genre string, tahun int64, dataFilm *[]movie) {
	var data movie
	data = movie{name, genre, waktu, tahun}
	*dataFilm = append(*dataFilm, data)

}

func main() {

	// Soal 1
	var dataBuah = buah{}
	dataBuah.Nama = []string{"Nanas", "Jeruk", "Semangka", "Pisang"}
	dataBuah.Warna = []string{"Kuning", "Oranye", "Hijau & Merah", "Kuning"}
	dataBuah.AdaBijinya = []bool{false, true, true, false}
	dataBuah.Harga = []int64{9000, 8000, 10000, 5000}

	for i := 0; i < len(dataBuah.Nama); i++ {
		fmt.Println("Nama :", dataBuah.Nama[i])
		fmt.Println("Warna :", dataBuah.Warna[i])
		if dataBuah.AdaBijinya[i] == true {
			fmt.Println("Ada Bijinya : Ada")
		} else {
			fmt.Println("Ada Bijinya : Tidak Ada")
		}
		fmt.Println("Harga :", dataBuah.Harga[i])
	}

	// Soal 2
	var dataSegitiga = segitiga{2, 3}
	var dataLuasPersegi = persegi{2}
	var dataLuasPersegiPanjang = persegiPanjang{2, 3}
	dataSegitiga.luasSegitiga()
	dataLuasPersegi.luasPersegi()
	dataLuasPersegiPanjang.luasPersegiPanjang()

	// Soal 3
	var dataWarna = phone{}
	var color = []string{"merah", "kuning"}
	dataWarna.colorsProperty(color)
	fmt.Println(dataWarna.colors)

	// Soal 4
	var dataFilm = []movie{}

	tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
	tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
	tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
	tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)

	for index, item := range dataFilm {
		fmt.Printf("%d. nama : %s\n", index+1, item.name)
		fmt.Println("  ", "duration :", item.jam/60, "Jam")
		fmt.Println("  ", "genre :", item.genre)
		fmt.Println("  ", "year :", item.tahun)

	}

}
