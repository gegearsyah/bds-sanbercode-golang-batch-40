package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

type NilaiMahasiswa struct {
	Nama        string `json:"nama"`
	MataKuliah  string `json:"mata_kuliah"`
	IndeksNilai string `json:"indeks_nilai"`
	Nilai       uint   `json:"nilai,string"`
	ID          uint   `json:"id,string"`
}

var nilaiNilaiMahasiswa = []NilaiMahasiswa{}

// Atur Basic Authentication
func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		uname, pwd, ok := r.BasicAuth()
		if !ok {
			w.Write([]byte("Username atau Password tidak boleh kosong"))
			return
		}

		if uname == "admin" && pwd == "admin" {
			next.ServeHTTP(w, r)
			return
		}
		w.Write([]byte("Username atau Password tidak sesuai"))
		return
	})
}

// Atur Post Endpoint
func createNilaiMahasiswa(w http.ResponseWriter, r *http.Request) {
	var nilaiMahasiswa NilaiMahasiswa
	if r.Method == "POST" {
		if r.Header.Get("Content-Type") == "application/json" {
			// parse dari json
			decodeJSON := json.NewDecoder(r.Body)
			if err := decodeJSON.Decode(&nilaiMahasiswa); err != nil {
				log.Fatal(err)
			}

			if nilaiMahasiswa.Nilai >= 80 && nilaiMahasiswa.Nilai <= 100 {
				nilaiMahasiswa.IndeksNilai = "A"
			} else if nilaiMahasiswa.Nilai < 80 && nilaiMahasiswa.Nilai >= 70 {
				nilaiMahasiswa.IndeksNilai = "B"
			} else if nilaiMahasiswa.Nilai < 70 && nilaiMahasiswa.Nilai >= 60 {
				nilaiMahasiswa.IndeksNilai = "C"
			} else if nilaiMahasiswa.Nilai < 60 && nilaiMahasiswa.Nilai >= 50 {
				nilaiMahasiswa.IndeksNilai = "D"
			} else if nilaiMahasiswa.Nilai < 50 && nilaiMahasiswa.Nilai >= 0 {
				nilaiMahasiswa.IndeksNilai = "E"
			} else {
				http.Error(w, "Masukkan Nilai yang Sesuai (0-100)", http.StatusBadRequest)
				return
			}

			if len(nilaiNilaiMahasiswa) == 0 {
				nilaiMahasiswa.ID = 1
			} else {
				nilaiMahasiswa.ID = uint(len(nilaiNilaiMahasiswa)) + 1
			}

			nilaiNilaiMahasiswa = append(nilaiNilaiMahasiswa, nilaiMahasiswa)

		} else {
			// parse dari form
			nama := r.PostFormValue("nama")
			mataKuliah := r.PostFormValue("mata_kuliah")
			getNilai := r.PostFormValue("nilai")
			nilai, _ := strconv.Atoi(getNilai)
			var indeksNilai string
			var id uint

			if nilai >= 80 && nilai <= 100 {
				indeksNilai = "A"
			} else if nilai < 80 && nilai >= 70 {
				indeksNilai = "B"
			} else if nilai < 70 && nilai >= 60 {
				indeksNilai = "C"
			} else if nilai < 60 && nilai >= 50 {
				indeksNilai = "D"
			} else if nilai < 50 && nilai >= 0 {
				indeksNilai = "E"
			} else {
				http.Error(w, "Masukkan Nilai yang Sesuai (0-100)", http.StatusBadRequest)
				return
			}

			if len(nilaiNilaiMahasiswa) == 0 {
				id = 1
			} else {
				id = uint(len(nilaiNilaiMahasiswa)) + 1
			}

			nilaiMahasiswa = NilaiMahasiswa{
				Nama:        nama,
				MataKuliah:  mataKuliah,
				IndeksNilai: indeksNilai,
				Nilai:       uint(nilai),
				ID:          id,
			}

			nilaiNilaiMahasiswa = append(nilaiNilaiMahasiswa, nilaiMahasiswa)
		}

		dataNilai, _ := json.Marshal(nilaiMahasiswa) // to byte
		w.Write(dataNilai)                           // cetak di browser
		return
	}
	http.Error(w, "NOT FOUND", http.StatusNotFound)
	return
}

// Atur Get Endpoint
func getNilai(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		dataMahasiswa, err := json.Marshal(nilaiNilaiMahasiswa)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(dataMahasiswa)
		return
	}
	http.Error(w, "ERROR....", http.StatusNotFound)
}

func main() {
	http.Handle("/buat-nilai", Auth(http.HandlerFunc(createNilaiMahasiswa)))
	http.HandleFunc("/nilai", getNilai)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Server Berjalan. Silahkan Pergi ke /nilai untuk mendapatkan data mahasiswa dan kirim data ke /buat-nilai untuk membuat data mahasiswa baru")
		fmt.Fprintln(w, "Jangan Lupa untuk Menggunakan Basic Authentication untuk menggunakan endpoints")
	})

	fmt.Println("server running at http://localhost:8080")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}

}
