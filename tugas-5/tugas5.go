package main

import "fmt"

// Soal 1
func luasPersegiPanjang(panjang int, lebar int) int {
	result := panjang * lebar
	return result
}
func kelilingPersegiPanjang(panjang int, lebar int) int {
	result := (2 * panjang) + (2 * lebar)
	return result

}
func volumeBalok(panjang int, lebar int, tinggi int) int {
	result := panjang * lebar * tinggi
	return result
}

// Soal 2
func introduce(nama string, jk string, pekerjaan string, usia string) (kalimat string) {
	if jk == "laki-laki" {
		kalimat = "Pak " + nama + " adalah " + "seorang " + pekerjaan + " yang berusia " + usia + " tahun"
	} else if jk == "perempuan" {
		kalimat = "Bu " + nama + " adalah " + "seorang " + pekerjaan + " yang berusia " + usia + " tahun"
	}

	return
}

// Soal 3
func buahFavorit(nama string, buah ...string) string {
	kalimat := "halo nama saya " + nama + " dan buah favorit saya adalah "

	for index, value := range buah {
		if index+1 == len(buah) {
			kalimat = kalimat + `"` + value + `"`
		} else {
			kalimat = kalimat + `"` + value + `", `
		}

	}

	return kalimat

}

// Soal 4

func main() {
	// Soal 1
	panjang := 12
	lebar := 4
	tinggi := 8

	luas := luasPersegiPanjang(panjang, lebar)
	keliling := kelilingPersegiPanjang(panjang, lebar)
	volume := volumeBalok(panjang, lebar, tinggi)

	fmt.Println(luas)
	fmt.Println(keliling)
	fmt.Println(volume)

	// Soal 2
	john := introduce("John", "laki-laki", "penulis", "30")
	fmt.Println(john) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

	sarah := introduce("Sarah", "perempuan", "model", "28")
	fmt.Println(sarah) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	// Soal 3

	var buah = []string{"semangka", "jeruk", "melon", "pepaya"}

	var buahFavoritJohn = buahFavorit("John", buah...)

	fmt.Println(buahFavoritJohn)
	// halo nama saya john dan buah favorit saya adalah "semangka", "jeruk", "melon", "pepaya"

	// Soal 4

	var dataFilm = []map[string]string{}
	// buatlah closure function disini
	var tambahDataFilm = func(film string, waktu string, genre string, tahun string) {
		data := map[string]string{}

		data["genre"] = genre
		data["jam"] = waktu
		data["tahun"] = tahun
		data["title"] = film

		dataFilm = append(dataFilm, data)

	}

	tambahDataFilm("LOTR", "2 jam", "action", "1999")
	tambahDataFilm("avenger", "2 jam", "action", "2019")
	tambahDataFilm("spiderman", "2 jam", "action", "2004")
	tambahDataFilm("juon", "2 jam", "horror", "2004")

	for _, item := range dataFilm {
		fmt.Println(item)
	}

}
