package main

import "fmt"

func main() {

	// Soal 1
	for i := 1; i <= 20; i++ {
		if i%2 == 0 {
			fmt.Println("Berkualitas")
		} else if i%2 != 0 {
			if i%3 == 0 {
				fmt.Println("I Love Coding")
			} else {
				fmt.Println("Santai")
			}

		}
	}

	// Soal 2
	var pagar = "#"

	for i := 0; i < 8; i++ {
		fmt.Println(pagar)
		pagar = pagar + "#"
	}

	// Soal 3
	var kalimat = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}
	newKalimat := kalimat[2:7]

	fmt.Println(newKalimat)

	// Soal 4
	var sayuran = []string{}
	var newSayur = []string{"Bayam", "Buncis", "Kangkung", "Kubis", "Seledri", "Tauge", "Timun"}

	for _, value := range newSayur {
		sayuran = append(sayuran, value)
	}

	for index, value := range sayuran {
		index = index + 1
		fmt.Printf("%d.%s \n", index, value)
	}

	// Soal 5
	var satuan = map[string]int{
		"panjang": 7,
		"lebar":   4,
		"tinggi":  6,
	}

	var volume int = 1

	for index, value := range satuan {
		fmt.Printf("%s = %d \n", index, value)
		volume = volume * value
	}

	fmt.Printf("Volume Balok = %d \n", volume)

}
