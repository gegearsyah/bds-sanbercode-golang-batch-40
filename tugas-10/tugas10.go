package main

import (
	"fmt"
	"strconv"
	"time"
)

func sentence(kalimat string, tahun int) {
	fmt.Println(kalimat, tahun)
}

// Soal 2
func errorKelilingSegitigaSamaSisi() {
	message := recover()
	fmt.Println("Terjadi Error :", message)
}
func kelilingSegitigaSamaSisi(angka int, boolean bool) interface{} {
	var data interface{}
	if boolean == true {
		if angka == 0 {
			data = "Maaf anda belum menginput sisi dari segitiga sama sisi"
		} else {
			data = "Keliling segitiga sama sisi dengan sisinya " + strconv.Itoa(angka) + " cm adalah " + strconv.Itoa(angka*3) + " cm"
		}
	} else {
		if angka == 0 {
			defer errorKelilingSegitigaSamaSisi()
			panic("Maaf anda belum menginput sisi dari segitiga sama sisi")

		} else {
			data = angka
		}
	}

	return data
}

// Soal 3
func tambahAngka(angka1 int, angka2 *int) {
	*angka2 = *angka2 + angka1
}

func cetakAngka(angka *int) {
	fmt.Println("Total Angka :", *angka)
}

// Soal 4
func tambahPhone(phone string, phones *[]string) {
	*phones = append(*phones, phone)
}

func main() {
	// deklarasi variabel angka ini simpan di baris pertama func main
	angka := 1

	// Soal 1
	var kalimat = "Golang Backend Development"
	var tahun = 2021
	defer sentence(kalimat, tahun)

	// Soal 2

	fmt.Println(kelilingSegitigaSamaSisi(4, true))

	fmt.Println(kelilingSegitigaSamaSisi(8, false))

	fmt.Println(kelilingSegitigaSamaSisi(0, true))

	fmt.Println(kelilingSegitigaSamaSisi(0, false))

	// Soal 3

	defer cetakAngka(&angka)

	tambahAngka(7, &angka)

	tambahAngka(6, &angka)

	tambahAngka(-1, &angka)

	tambahAngka(9, &angka)

	// Soal 4
	var phones = []string{}
	tambahPhone("Xiaomi", &phones)
	tambahPhone("Asus", &phones)
	tambahPhone("IPhone", &phones)
	tambahPhone("Samsung", &phones)
	tambahPhone("Oppo", &phones)
	tambahPhone("Realme", &phones)
	tambahPhone("Vivo", &phones)

	for index, value := range phones {
		nomor := index + 1
		fmt.Printf("%d. %s\n", nomor, value)
		time.Sleep(time.Second * 1)
	}

}
