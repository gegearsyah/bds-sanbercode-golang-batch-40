package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// Soal 1 //
	var variabel1 = "Bootcamp"
	var variabel2 = "Digital"
	var variabel3 = "Skill"
	var variabel4 = "Sanbercode"
	var variabel5 = "Golang"

	var soal1 = variabel1 + " " + variabel2 + " " + variabel3 + " " + variabel4 + " " + variabel5
	fmt.Println(soal1)

	// Soal 2 //

	halo := "Halo Dunia"

	var soal2 = strings.Replace(halo, "Dunia", "Golang", -1)
	fmt.Println(soal2)

	// Soal 3 //

	var kataPertama = "saya"
	var kataKedua = "senang"
	var kataKetiga = "belajar"
	var kataKeempat = "golang"

	kataKetiga = strings.Replace(kataKetiga, "r", "R", 1)
	kataKeempat = strings.Replace(kataKeempat, "golang", "GOLANG", -1)

	var soal3 = kataPertama + " " + kataKedua + " " + kataKetiga + " " + kataKeempat
	fmt.Println(soal3)

	// Soal 4 //

	var angkaPertama = "8"
	var angkaKedua = "5"
	var angkaKetiga = "6"
	var angkaKeempat = "7"

	Pertama, _ := strconv.Atoi(angkaPertama)
	Kedua, _ := strconv.Atoi(angkaKedua)
	Ketiga, _ := strconv.Atoi(angkaKetiga)
	Keempat, _ := strconv.Atoi(angkaKeempat)
	var soal4 = Pertama + Kedua + Ketiga + Keempat

	fmt.Println(soal4)

	// Soal 5 //

	kalimat := "halo halo bandung"
	angka := 2021

	kalimat = strings.Replace(kalimat, "halo", "Hi", 2)

	fmt.Printf(`"%s" - %d`, kalimat, angka)

}
