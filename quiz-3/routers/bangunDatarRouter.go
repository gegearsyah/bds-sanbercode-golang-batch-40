package routers

import (
	"quiz3/controllers"

	"github.com/gin-gonic/gin"
)

func (r routes) RouteBangunDatar(rg *gin.RouterGroup) {
	bangunDatar := rg.Group("/bangun-datar")

	bangunDatar.GET("/segitiga-sama-sisi", controllers.HitungSegitigaSamaSisi)
	bangunDatar.GET("/persegi", controllers.HitungPersegi)
	bangunDatar.GET("/persegi-panjang", controllers.HitungPersegiPanjang)
	bangunDatar.GET("/lingkaran", controllers.HitungLingkaran)
	bangunDatar.GET("/jajar-genjang", controllers.HitungJajarGenjang)

}
