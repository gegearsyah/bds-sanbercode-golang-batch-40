package routers

import (
	"quiz3/controllers"

	"github.com/gin-gonic/gin"
)

func (r routes) RouteCategory(rg *gin.RouterGroup) {
	category := rg.Group("/categories")

	category.GET("/", controllers.GetCategory)
	category.POST("/", basicAuth, controllers.CreateCategory)
	category.PUT("/:id", basicAuth, controllers.UpdateCategory)
	category.DELETE("/:id", basicAuth, controllers.DeleteCategory)
	category.GET("/:id/books", controllers.GetBookWithCategories)
}
