package routers

import (
	"quiz3/controllers"

	"github.com/gin-gonic/gin"
)

func (r routes) RouteBook(rg *gin.RouterGroup) {
	book := rg.Group("/books")
	book.GET("/", controllers.GetBook)
	book.POST("/", basicAuth, controllers.CreateBook)
	book.PUT("/:id", basicAuth, controllers.UpdateBook)
	book.DELETE("/:id", basicAuth, controllers.DeleteBook)
}
