package routers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type routes struct {
	router *gin.Engine
}

func basicAuth(ctx *gin.Context) {
	// Get the Basic Authentication credentials
	user, password, hasAuth := ctx.Request.BasicAuth()
	if hasAuth && user == "admin" && password == "password" {
		ctx.Next()
	} else if hasAuth && user == "editor" && password == "secret" {
		ctx.Next()
	} else {
		ctx.Abort()
		ctx.Writer.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		ctx.JSON(http.StatusNonAuthoritativeInfo, gin.H{
			"Error:": "Silahkan Masukan user dan password yang benar",
		})

		return
	}
}

func NewRoutes() routes {
	r := routes{
		router: gin.Default(),
	}

	home := r.router.Group("/")

	r.RouteBangunDatar(home)
	r.RouteCategory(home)
	r.RouteBook(home)

	return r
}

func (r routes) Run(addr ...string) error {
	return r.router.Run()
}
