PGDMP     6    -                z            db-quiz3    11.18    15.0     
           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    16408    db-quiz3    DATABASE     �   CREATE DATABASE "db-quiz3" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';
    DROP DATABASE "db-quiz3";
                postgres    false                        2615    2200    public    SCHEMA     2   -- *not* creating schema, since initdb creates it
 2   -- *not* dropping schema, since initdb creates it
                postgres    false                       0    0    SCHEMA public    ACL     Q   REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;
                   postgres    false    6            �            1259    16421    books    TABLE     �  CREATE TABLE public.books (
    id integer NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    image_url character varying NOT NULL,
    release_year integer NOT NULL,
    price integer NOT NULL,
    total_page integer NOT NULL,
    thickness character varying(50) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    category_id integer NOT NULL
);
    DROP TABLE public.books;
       public            postgres    false    6            �            1259    16419    books_id_seq    SEQUENCE     �   CREATE SEQUENCE public.books_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.books_id_seq;
       public          postgres    false    6    199                       0    0    books_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.books_id_seq OWNED BY public.books.id;
          public          postgres    false    198            �            1259    16411 
   categories    TABLE     �   CREATE TABLE public.categories (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
    DROP TABLE public.categories;
       public            postgres    false    6            �            1259    16409    categories_id_seq    SEQUENCE     �   CREATE SEQUENCE public.categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.categories_id_seq;
       public          postgres    false    197    6                       0    0    categories_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;
          public          postgres    false    196            �
           2604    16424    books id    DEFAULT     d   ALTER TABLE ONLY public.books ALTER COLUMN id SET DEFAULT nextval('public.books_id_seq'::regclass);
 7   ALTER TABLE public.books ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    199    198    199            �
           2604    16414    categories id    DEFAULT     n   ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);
 <   ALTER TABLE public.categories ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    196    197    197                      0    16421    books 
   TABLE DATA           �   COPY public.books (id, title, description, image_url, release_year, price, total_page, thickness, created_at, updated_at, category_id) FROM stdin;
    public          postgres    false    199   �                 0    16411 
   categories 
   TABLE DATA           F   COPY public.categories (id, name, created_at, updated_at) FROM stdin;
    public          postgres    false    197                     0    0    books_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.books_id_seq', 2, true);
          public          postgres    false    198                       0    0    categories_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.categories_id_seq', 2, true);
          public          postgres    false    196            �
           2606    16429    books books_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.books DROP CONSTRAINT books_pkey;
       public            postgres    false    199            �
           2606    16416    categories categories_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.categories DROP CONSTRAINT categories_pkey;
       public            postgres    false    197            �
           2606    16440    books books_category_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.categories(id) DEFERRABLE NOT VALID;
 F   ALTER TABLE ONLY public.books DROP CONSTRAINT books_category_id_fkey;
       public          postgres    false    197    199    2695                 x��MN�0���)|���I���X ���"Ă͸q��ǎbSҞ�+�p����4�[�����D�]��w�^�����d��G�gڌ�,bϮ�΄�؎Frr�t��Ƒ�!m�{���H�SfcB+�`"���@���A�F?��T�}��|,�����>�[��8���0A[Vj*$�Z�� DG��`}�A@#�j]*mP)�eɷ�&C �X�L�̂�ҧ� ��ELʶH��d�'��i�sĲNQ�7�V��neͫ��
�o��<� dK�         :   x�3�LL/�K�4202�5!C+c3+=KCs#3t)s+K=3KS�=... �I     