package main

import "quiz3/routers"

func main() {
	var port = ":8080"
	routes := routers.NewRoutes()
	routes.Run(port)
}
