package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func HitungSegitigaSamaSisi(ctx *gin.Context) {
	getAlas := ctx.Query("alas")
	fmt.Println(getAlas)
	alas, err := strconv.Atoi(getAlas)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	getTinggi := ctx.Query("tinggi")
	tinggi, err := strconv.Atoi(getTinggi)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	hitung := ctx.Query("hitung")

	var hasil int
	if hitung == "luas" {
		hasil = (alas * tinggi)
		ctx.JSON(http.StatusOK, gin.H{
			"results": fmt.Sprintf("Luas Segitiga Sama Sisi = %v", hasil),
		})
		return
	} else if hitung == "keliling" {
		hasil = 3 * alas
		ctx.JSON(http.StatusOK, gin.H{
			"results": fmt.Sprintf("Keliling Segitiga Sama Sisi = %v", hasil),
		})
		return
	} else {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error_status":  "Hitung Not Found",
			"error_message": fmt.Sprintln("Hitung Hanya tersedia untuk luas dan keliling"),
		})
		return
	}

}

func HitungPersegiPanjang(ctx *gin.Context) {
	getPanjang := ctx.Query("panjang")
	panjang, err := strconv.Atoi(getPanjang)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	getLebar := ctx.Query("lebar")
	lebar, err := strconv.Atoi(getLebar)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	hitung := ctx.Query("hitung")

	var hasil int
	if hitung == "luas" {
		hasil = panjang * lebar
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Luas Persegi Panjang",
			"results": hasil,
		})
		return
	} else if hitung == "keliling" {
		hasil = (2 * panjang) + (2 * lebar)
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Keliling Persegi Panjang",
			"results": hasil,
		})
		return
	} else {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error_status":  "Hitung Not Found",
			"error_message": fmt.Sprintln("Hitung Hanya tersedia untuk luas dan keliling"),
		})
		return
	}

}

func HitungPersegi(ctx *gin.Context) {
	getSisi := ctx.Query("sisi")
	sisi, err := strconv.Atoi(getSisi)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	hitung := ctx.Query("hitung")

	var hasil int
	if hitung == "luas" {
		hasil = sisi * sisi
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Luas Persegi",
			"results": hasil,
		})
		return
	} else if hitung == "keliling" {
		hasil = 4 * sisi
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Keliling Persegi",
			"results": hasil,
		})
		return
	} else {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error_status":  "Hitung Not Found",
			"error_message": fmt.Sprintln("Hitung Hanya tersedia untuk luas dan keliling"),
		})
		return

	}

}

func HitungLingkaran(ctx *gin.Context) {
	getJariJari := ctx.Query("jariJari")
	jariJari, err := strconv.Atoi(getJariJari)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	hitung := ctx.Query("hitung")

	var hasil int
	if hitung == "luas" {
		hasil = 22 / 7 * jariJari * jariJari
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Luas Lingkaran",
			"results": hasil,
		})
		return
	} else if hitung == "keliling" {
		hasil = 2 * 22 / 7 * jariJari
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Keliling Lingkaran",
			"results": hasil,
		})
		return
	} else {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error_status":  "Hitung Not Found",
			"error_message": fmt.Sprintln("Hitung Hanya tersedia untuk luas dan keliling"),
		})
		return
	}

}

func HitungJajarGenjang(ctx *gin.Context) {
	getAlas := ctx.Query("alas")
	alas, err := strconv.Atoi(getAlas)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	getTinggi := ctx.Query("tinggi")
	tinggi, err := strconv.Atoi(getTinggi)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	getSisi := ctx.Query("sisi")
	sisi, err := strconv.Atoi(getSisi)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	hitung := ctx.Query("hitung")

	var hasil int
	if hitung == "luas" {
		hasil = alas * tinggi
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Luas Jajar Genjang",
			"results": hasil,
		})
		return
	} else if hitung == "keliling" {
		hasil = (2 * alas) + (2 * sisi)
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Keliling Jajar Genjang",
			"results": hasil,
		})
		return
	} else {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error_status":  "Hitung Not Found",
			"error_message": fmt.Sprintln("Hitung Hanya tersedia untuk luas dan keliling"),
		})
		return

	}

}
