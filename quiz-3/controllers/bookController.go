package controllers

import (
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/gin-gonic/gin"
)

type Book struct {
	Id           int
	Title        string `json:"title"`
	Description  string `json:"description"`
	Image_url    string `json:"image_url"`
	Release_year int    `json:"release_year"`
	Price        string `json:"price"`
	Total_page   int    `json:"total_page"`
	Thickness    string
	Created_at   time.Time
	Updated_at   time.Time
	Category_id  int `json:"category_id"`
}

func CreateBook(ctx *gin.Context) {
	var newBook Book
	if err := ctx.ShouldBindJSON(&newBook); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	if newBook.Release_year <= 1980 || newBook.Release_year >= 2021 {

		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Silahkan Masukkan Tahun Yang Sesuai (1980-2021)",
		})
		return
	}

	var created_at = time.Now()
	var updated_at = time.Now()
	var thickness string
	_, err = url.ParseRequestURI(newBook.Image_url)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	// Validasi Jumlah Halaman
	if newBook.Total_page <= 100 && newBook.Total_page > 0 {
		thickness = "tipis"
	} else if newBook.Total_page <= 200 && newBook.Total_page > 100 {
		thickness = "sedang"
	} else if newBook.Total_page > 200 {
		thickness = "tebal"
	} else {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Jumlah Halaman Tidak Valid",
		})
		return
	}

	sqlStatement := `
	INSERT INTO books (title, description, image_url,release_year,price,total_page,thickness,created_at,updated_at,category_id)
	VALUES ($1, $2, $3,$4,$5,$6,$7,$8,$9,$10)
	Returning *
	`

	err = db.QueryRow(sqlStatement, &newBook.Title, &newBook.Description, &newBook.Image_url, &newBook.Release_year, &newBook.Price,
		&newBook.Total_page, thickness, created_at, updated_at, &newBook.Category_id).
		Scan(&newBook.Id, &newBook.Title, &newBook.Description, &newBook.Image_url, &newBook.Release_year, &newBook.Price,
			&newBook.Total_page, &newBook.Thickness, &newBook.Created_at, &newBook.Updated_at, &newBook.Category_id)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"new_data": newBook,
	})

}

func GetBook(ctx *gin.Context) {
	var results = []Book{}

	sqlStatement := `SELECT * from books`
	rows, err := db.Query(sqlStatement)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	defer rows.Close()

	for rows.Next() {

		var book = Book{}

		err = rows.Scan(&book.Id, &book.Title, &book.Description, &book.Image_url, &book.Release_year, &book.Price,
			&book.Total_page, &book.Thickness, &book.Created_at, &book.Updated_at, &book.Category_id)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": fmt.Sprintf("%v", err),
			})
			return
		}

		results = append(results, book)
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": results,
	})

}

func GetBookWithCategories(ctx *gin.Context) {
	var results = []Book{}
	getID := ctx.Param("id")
	sqlStatement := `
	SELECT B.*
	FROM books B
	INNER JOIN categories c ON c.id = $1
	`
	rows, err := db.Query(sqlStatement, getID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	defer rows.Close()

	for rows.Next() {

		var book = Book{}

		err = rows.Scan(&book.Id, &book.Title, &book.Description, &book.Image_url, &book.Release_year, &book.Price,
			&book.Total_page, &book.Thickness, &book.Created_at, &book.Updated_at, &book.Category_id)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": fmt.Sprintf("%v", err),
			})
			return
		}

		results = append(results, book)
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": results,
	})

}

func UpdateBook(ctx *gin.Context) {
	var book Book

	if err := ctx.ShouldBindJSON(&book); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	_, err = url.ParseRequestURI(book.Image_url)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
	}

	if book.Release_year <= 1980 || book.Release_year >= 2021 {

		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Silahkan Masukkan Tahun Yang Sesuai (1980-2021)",
		})
		return
	}
	var updated_at = time.Now()
	var thickness string

	// Validasi Jumlah Halaman
	if book.Total_page <= 100 && book.Total_page > 0 {
		thickness = "tipis"
	} else if book.Total_page <= 200 && book.Total_page > 100 {
		thickness = "sedang"
	} else if book.Total_page > 200 {
		thickness = "tebal"
	} else {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Jumlah Halaman Tidak Valid",
		})
		return
	}

	getID := ctx.Param("id")

	sqlStatement := `
	UPDATE books
	SET title = $2, description = $3, image_url= $4, release_year= $5, price= $6, total_page= $7, thickness= $8, updated_at= $9,category_id= $10
	WHERE  id  =  $1;
	`
	res, err := db.Exec(sqlStatement, getID, book.Title, book.Description, book.Image_url, book.Release_year, book.Price,
		book.Total_page, thickness, updated_at, book.Category_id)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	count, err := res.RowsAffected()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Updated data amount": fmt.Sprintln(count),
	})

}

func DeleteBook(ctx *gin.Context) {
	getID := ctx.Param("id")

	sqlStatement := `
	DELETE from books WHERE id =$1;
	`
	res, err := db.Exec(sqlStatement, getID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	count, err := res.RowsAffected()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Deleted data amount": fmt.Sprintln(count),
	})
}
