package controllers

import (
	"fmt"
	"net/http"
	"quiz3/databases"
	"time"

	"github.com/gin-gonic/gin"
)

type Category struct {
	Id         int
	Name       string `json:"name"`
	Created_at time.Time
	Updated_at time.Time
}

var (
	err error
	db  = databases.Database()
)

func CreateCategory(ctx *gin.Context) {
	var newCategory Category
	if err := ctx.ShouldBindJSON(&newCategory); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}
	var created_at = time.Now()
	var updated_at = time.Now()

	sqlStatement := `
	INSERT INTO categories (name, created_at, updated_at)
	VALUES ($1, $2, $3)
	Returning *
	`

	err = db.QueryRow(sqlStatement, &newCategory.Name, created_at, updated_at).
		Scan(&newCategory.Id, &newCategory.Name, &newCategory.Created_at, &newCategory.Updated_at)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"new_data": newCategory,
	})

}

func GetCategory(ctx *gin.Context) {
	var results = []Category{}

	sqlStatement := `SELECT * from categories`
	rows, err := db.Query(sqlStatement)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	defer rows.Close()

	for rows.Next() {

		var category = Category{}

		err = rows.Scan(&category.Id, &category.Name, &category.Created_at, &category.Updated_at)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": fmt.Sprintf("%v", err),
			})
			return
		}

		results = append(results, category)
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": results,
	})

}

func UpdateCategory(ctx *gin.Context) {
	var category Category

	if err := ctx.ShouldBindJSON(&category); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var updated_at = time.Now()
	getID := ctx.Param("id")

	sqlStatement := `
	UPDATE categories
	SET name = $2, updated_at = $3
	WHERE  id  =  $1;
	`
	res, err := db.Exec(sqlStatement, getID, category.Name, updated_at)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}
	count, err := res.RowsAffected()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Updated data amount": fmt.Sprintln(count),
	})

}

func DeleteCategory(ctx *gin.Context) {
	getID := ctx.Param("id")

	sqlStatement := `
	DELETE from categories WHERE id =$1;
	`
	res, err := db.Exec(sqlStatement, getID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	count, err := res.RowsAffected()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("%v", err),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Deleted data amount": fmt.Sprintln(count),
	})
}
