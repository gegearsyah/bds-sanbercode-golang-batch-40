package main

import (
	"fmt"
	"strconv"
	"strings"
)

// Soal 1
type segitigaSamaSisi struct {
	alas, tinggi int
}

func (s segitigaSamaSisi) luas() int {
	luas := (s.alas * s.tinggi)

	fmt.Println("Luas Segitiga Sama Sisi :", luas)

	return luas
}

func (s segitigaSamaSisi) keliling() int {
	keliling := 3 * s.alas

	fmt.Println("Keliling Segitiga Sama Sisi :", keliling)
	return keliling
}

type persegiPanjang struct {
	panjang, lebar int
}

func (p persegiPanjang) luas() int {
	luas := p.panjang * p.lebar
	fmt.Println("Luas Persegi Panjang :", luas)
	return luas

}

func (p persegiPanjang) keliling() int {
	keliling := (2 * p.panjang) + (2 * p.lebar)
	fmt.Println("Keliling Persegi Panjang :", keliling)
	return keliling
}

type tabung struct {
	jariJari, tinggi float64
}

func (t tabung) volume() float64 {
	volume := (22 * t.jariJari * t.jariJari * t.tinggi) / 7
	fmt.Println("Volume Tabung :", volume)
	return volume
}

func (t tabung) luasPermukaan() float64 {
	luasPermukaan := (2 * 22 * t.jariJari * (t.jariJari + t.tinggi)) / 7
	fmt.Println("Luas Permukaan Tabung :", luasPermukaan)
	return luasPermukaan
}

type balok struct {
	panjang, lebar, tinggi int
}

func (b balok) volume() float64 {
	volume := b.lebar * b.panjang * b.tinggi
	fmt.Println("Volume Balok :", volume)
	return float64(volume)
}

func (b balok) luasPermukaan() float64 {
	luasPermukaan := 2 * ((b.panjang * b.lebar) + (b.lebar * b.tinggi) + (b.panjang * b.tinggi))
	fmt.Println("Luas Permukaan Balok :", luasPermukaan)
	return float64(luasPermukaan)
}

type hitungBangunDatar interface {
	luas() int
	keliling() int
}

type hitungBangunRuang interface {
	volume() float64
	luasPermukaan() float64
}

// Soal 2
type phone struct {
	name, brand string
	year        int
	colors      []string
}

type brandPrint interface {
	cetakHandphone() string
}

func (p phone) cetakHandphone() string {
	name := "name : " + p.name
	brand := "brand : " + p.brand
	year := "year : " + strconv.Itoa(p.year)
	colors := "colors : "
	for index, value := range p.colors {
		if index == len(p.colors)-1 {
			colors = colors + value
		} else {
			colors = colors + value + ","
		}

	}

	return fmt.Sprintf("%s\n%s\n%s\n%s\n", name, brand, year, colors)

}

func luasPersegi(angka int, boolean bool) interface{} {

	var data interface{}
	if boolean == true {
		if angka == 0 {
			data = "Maaf anda belum menginput sisi dari persegi"
		} else {
			data = "luas persegi dengan sisi " + strconv.Itoa(angka) + " cm adalah " + strconv.Itoa(angka*angka) + " cm"
		}
	} else {
		if angka == 0 {
			data = nil
		} else {
			data = angka
		}
	}
	return data
}

func main() {

	// Soal 1
	var dataSegitigaSamaSisi hitungBangunDatar = segitigaSamaSisi{12, 13}
	dataSegitigaSamaSisi.luas()
	dataSegitigaSamaSisi.keliling()

	var dataPersegiPanjang hitungBangunDatar = persegiPanjang{12, 13}
	dataPersegiPanjang.luas()
	dataPersegiPanjang.keliling()

	var dataTabung hitungBangunRuang = tabung{12, 13}
	dataTabung.volume()
	dataTabung.luasPermukaan()

	var dataBalok hitungBangunRuang = balok{12, 13, 3}
	dataBalok.volume()
	dataBalok.luasPermukaan()

	// Soal 2
	var dataPhone brandPrint = phone{"Samsung Galaxy Note 20", "Samsung Galaxy Note 20", 200, []string{"Mystic Bronze", "Mystic White", "Mystic Black"}}
	hasilCetak := dataPhone.cetakHandphone()
	fmt.Println(hasilCetak)

	// Soal 3
	fmt.Println(luasPersegi(4, true))
	fmt.Println(luasPersegi(8, false))
	fmt.Println(luasPersegi(0, true))
	fmt.Println(luasPersegi(0, false))

	// Soal 4
	var prefix interface{} = "hasil penjumlahan dari "

	var kumpulanAngkaPertama interface{} = []int{6, 8}

	var kumpulanAngkaKedua interface{} = []int{12, 14}

	// tulis jawaban anda disini
	sumAngkaPertama := 0
	sumAngkaKedua := 0

	for _, value := range kumpulanAngkaPertama.([]int) {
		sumAngkaPertama = sumAngkaPertama + value
	}

	for _, value := range kumpulanAngkaKedua.([]int) {
		sumAngkaKedua = sumAngkaKedua + value
	}

	hitungAngkaPertama := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(kumpulanAngkaPertama)), " + "), "[]")
	hitungAngkaKedua := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(kumpulanAngkaKedua)), " + "), "[]")

	fmt.Println(prefix, hitungAngkaPertama, "+", hitungAngkaKedua, "=", sumAngkaPertama+sumAngkaKedua)
}
