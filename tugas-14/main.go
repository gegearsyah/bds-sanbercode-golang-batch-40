package main

import "tugas14/routers"

func main() {
	var port = ":8080"
	routers.StartServer().Run(port)
}
