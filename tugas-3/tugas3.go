package main

import (
	"fmt"
	"strconv"
)

func main() {
	// Soal 1
	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"

	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"

	var luasPersegiPanjang int
	var kelilingPersegiPanjang int
	var luasSegitiga int

	panjangPersegi, _ := strconv.Atoi(panjangPersegiPanjang)
	lebarPersegi, _ := strconv.Atoi(lebarPersegiPanjang)
	alasSegi, _ := strconv.Atoi(alasSegitiga)
	tinggiSegi, _ := strconv.Atoi(tinggiSegitiga)

	luasPersegiPanjang = panjangPersegi * lebarPersegi
	fmt.Println(luasPersegiPanjang)

	kelilingPersegiPanjang = (2 * panjangPersegi) + (2 * lebarPersegi)
	fmt.Println(kelilingPersegiPanjang)

	luasSegitiga = (alasSegi * tinggiSegi) / 2
	fmt.Println(luasSegitiga)

	// Soal 2

	var nilaiJohn = 80
	var nilaiDoe = 50

	if nilaiJohn >= 80 {
		fmt.Println("A")
	} else if nilaiJohn < 80 && nilaiJohn >= 70 {
		fmt.Println("B")
	} else if nilaiJohn < 70 && nilaiJohn >= 60 {
		fmt.Println("C")
	} else if nilaiJohn < 60 && nilaiJohn >= 50 {
		fmt.Println("D")
	} else if nilaiJohn < 50 && nilaiJohn >= 0 {
		fmt.Println("E")
	}

	if nilaiDoe >= 80 {
		fmt.Println("A")
	} else if nilaiDoe < 80 && nilaiDoe >= 70 {
		fmt.Println("B")
	} else if nilaiDoe < 70 && nilaiDoe >= 60 {
		fmt.Println("C")
	} else if nilaiDoe < 60 && nilaiDoe >= 50 {
		fmt.Println("D")
	} else if nilaiDoe < 50 && nilaiDoe >= 0 {
		fmt.Println("E")
	}

	// Soal 3

	var tanggal = 11
	var bulan = 4
	var tahun = 2000

	switch bulan {
	case 1:
		fmt.Printf(`%d Januari %d`, tanggal, tahun)
	case 2:
		fmt.Printf(`%d Februari %d`, tanggal, tahun)
	case 3:
		fmt.Printf(`%d Maret %d`, tanggal, tahun)
	case 4:
		fmt.Printf(`%d April %d`, tanggal, tahun)
	case 5:
		fmt.Printf(`%d Mei %d`, tanggal, tahun)
	case 6:
		fmt.Printf(`%d Juni %d`, tanggal, tahun)
	case 7:
		fmt.Printf(`%d Juli %d`, tanggal, tahun)
	case 8:
		fmt.Printf(`%d Agustus %d`, tanggal, tahun)
	case 9:
		fmt.Printf(`%d September %d`, tanggal, tahun)
	case 10:
		fmt.Printf(`%d Oktober %d`, tanggal, tahun)
	case 11:
		fmt.Printf(`%d November %d`, tanggal, tahun)
	case 12:
		fmt.Printf(`%d Desember %d`, tanggal, tahun)
	}

	// Soal 4

	// Baby boomer, kelahiran 1944 s.d 1964
	// Generasi X, kelahiran 1965 s.d 1979
	// Generasi Y (Millenials), kelahiran 1980 s.d 1994
	// Generasi Z, kelahiran 1995 s.d 2015

	if tahun >= 1944 && tahun < 1965 {
		fmt.Println("Baby Boomer")
	} else if tahun >= 1965 && tahun < 1980 {
		fmt.Println("Generasi X")
	} else if tahun >= 1980 && tahun < 1995 {
		fmt.Println("Generasi Y")
	} else if tahun >= 1995 && tahun <= 2015 {
		fmt.Println("Generasi Z")
	}

}
