package main

import (
	"fmt"
	"net/http"
)

func volumeTabung(JariJari float64, Tinggi float64) float64 {
	Volume := (22 * JariJari * JariJari * Tinggi) / 7
	return Volume
}

func luasPermukaanTabung(JariJari float64, Tinggi float64) float64 {
	LuasPermukaan := (2 * 22 * JariJari * (JariJari + Tinggi)) / 7
	return LuasPermukaan
}

func kelilingPermukaanTabung(JariJari float64, Tinggi float64) float64 {
	KelilingPermukaan := (2 * 22 * JariJari) / 7
	return KelilingPermukaan
}
func main() {
	// Soal 4
	var jariJari float64 = 7
	var tinggi float64 = 10

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "jariJari : ", jariJari, ", tinggi: ", tinggi, ", volume : ", volumeTabung(jariJari, tinggi), ", luas alas: ", luasPermukaanTabung(jariJari, tinggi), ", keliling alas:", kelilingPermukaanTabung(jariJari, tinggi))
	})

	fmt.Println("starting web server at http://localhost:8080/")

	http.ListenAndServe(":8080", nil)

}
